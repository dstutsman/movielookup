//
//  AppDelegate.swift
//  MovieLookup
//
//  Created by Derek Stutsman on 5/16/19.
//  Copyright © 2019 Stutsman Software. All rights reserved.
//

import UIKit
import MovieLookupKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        //Hook the network activity counter from the kit
        MovieLookupKit.NetworkActivity.sharedInstance.display = {
            active in
            application.isNetworkActivityIndicatorVisible = active
        }

        return true
    }
    
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        //Clear the memory cache
        ImageClient.sharedInstance.clearCache()
    }
}

