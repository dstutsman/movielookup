//
//  SearchViewController.swift
//  MovieLookup
//
//  Created by Derek Stutsman on 5/16/19.
//  Copyright © 2019 Stutsman Software. All rights reserved.
//

import UIKit
import MovieLookupKit

class SearchViewController: UIViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var searchBar: UISearchBar?
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var noResultsLabel: UILabel?
    
    //MARK: - Properties
    var resultsPage = 1
    var lastSearchTerm : String?
    var lastSearchResult : SearchResult?
    var movies : [MovieSearchResult] = []
    
    //MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Disable prefetch on tableview
        self.tableView?.prefetchDataSource = nil
        
        //Make search bar focused
        self.searchBar?.becomeFirstResponder()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Pass movie ID to detail screen
        if (segue.identifier == "detailSegue") {
            //Crack all the optionals
            guard let cell: SearchResultTableViewCell = sender as? SearchResultTableViewCell,
                let indexPath = self.tableView?.indexPath(for: cell),
                let destinationController:MovieDetailsViewController = segue.destination as? MovieDetailsViewController else {
                return
            }
            
            //Deselect the row
            self.tableView?.deselectRow(at: indexPath, animated: true)
            
            //Assign the movie ID
            let movie = movies[indexPath.row]
            destinationController.movieID = movie.imdbID
        }
    }

    //MARK: - Private I/O
    private func performSearch(searchTerm: String) {
        //Ignore if search term is duplicate - prevents double-search for the same term when typing space, h
        if self.resultsPage == 1 && self.lastSearchTerm != nil && self.lastSearchTerm?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == searchTerm.trimmingCharacters(in: .whitespacesAndNewlines) {
            print("Ignoring duplicate search term")
            return
        }
        
        //Save search term for next time
        self.lastSearchTerm = searchTerm
        
        //Clear existing data on first page
        if self.resultsPage == 1 {
            self.movies.removeAll()
            self.tableView!.reloadData()
        }
        
        OMDBAPIClient.sharedInstance.search(searchTerm: searchTerm, page: self.resultsPage) {
            [weak self]
            (searchResult, error) in
            
            //Sanity check
            guard let strongSelf = self else {
                return
            }
            
            //Handle error?
            if error != nil {
                print("FETCH ERROR: \(error!)")
                strongSelf.tableView?.isHidden = true
                strongSelf.noResultsLabel?.isHidden = false
                //TODO: Differentiate between "no results" parsing errors, and actual network errors.
                //Display network errors with an alert
                return
            }
            
            //Sanity checks / crack optionals
            if let searchResult=searchResult {
                //Save search result
                strongSelf.lastSearchResult = searchResult
                
                //Is this the initial load?
                let firstLoad = strongSelf.resultsPage == 1
                
                //Add to the list, or clear it
                if firstLoad {
                    strongSelf.movies = searchResult.movies
                    strongSelf.tableView?.reloadData()
                } else {
                    strongSelf.movies.append(contentsOf: searchResult.movies)
                    strongSelf.tableView?.reloadData()
                }
                
                //Show tableView and hide "no results" label
                strongSelf.tableView?.isHidden = false
                strongSelf.noResultsLabel?.isHidden = true
            } else {
                //No results
                strongSelf.tableView?.isHidden = true
                strongSelf.noResultsLabel?.isHidden = false
            }
        }
    }
    
    //MARK: - UISearchBarDelegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //Reset data and perform fresh fetch
        if let searchTerm = searchBar.text {
            self.resultsPage = 1
            self.performSearch(searchTerm: searchTerm)
        }
        
        //Dismiss keyboard
        self.searchBar?.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //Capture existing searchbar text
        guard let searchTerm = searchBar.text else {
            return
        }
        
        //If it hasn't changed after 0.5 seconds, do the search
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if searchBar.text == searchTerm {
                self.resultsPage = 1
                self.performSearch(searchTerm: searchTerm)
            }
        }
    }
    
    //MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Dequeue the cell
        let cell:SearchResultTableViewCell = tableView.dequeueReusableCell(withIdentifier: SearchResultTableViewCell.identifier, for: indexPath) as! SearchResultTableViewCell
        
        //Grab the movie
        let movie = self.movies[indexPath.row]
        
        //Populate the cell
        cell.titleLabelView!.text = movie.title
        cell.yearLabelView!.text = movie.year
        cell.posterImageView!.image = UIImage(named:"placeholder")
        
        //Fetch the image, taking care to ensure it hasn't changed
        let posterURL = movie.posterURL
        ImageClient.sharedInstance.fetchImage(url: movie.posterURL) { (image, error) in
            if indexPath.row < self.movies.count {
                let currentMovie = self.movies[indexPath.row]
                if posterURL == currentMovie.posterURL && image != nil {
                    cell.posterImageView?.image = image
                    cell.posterImageView?.setNeedsDisplay()
                }
            }
        }
        
        //If we lack the data to fetch more, just bail
        guard let result = self.lastSearchResult,
            let count = Int(result.totalResults),
            let searchBar = self.searchBar,
            let searchTerm = searchBar.text else {
            return cell
        }
        
        //Fetch more on the last row
        if self.movies.count < count && indexPath.row == self.movies.count-1 {
            print("SearchViewController: Fetching more data")
            self.resultsPage += 1
            self.performSearch(searchTerm: searchTerm)
        }
        
        //Done
        return cell
    }

    
}
