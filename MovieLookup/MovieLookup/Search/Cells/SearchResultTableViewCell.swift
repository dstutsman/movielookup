//
//  SearchResultTableViewCell.swift
//  MovieLookup
//
//  Created by Derek Stutsman on 5/16/19.
//  Copyright © 2019 Stutsman Software. All rights reserved.
//

import UIKit

class SearchResultTableViewCell: UITableViewCell {
    //MARK: - Properties
    public static let identifier = "searchResultCell"
    @IBOutlet weak var posterImageView: UIImageView?
    @IBOutlet weak var titleLabelView: UILabel?
    @IBOutlet weak var yearLabelView: UILabel?
    
    //MARK: - UITableViewCell
    override func prepareForReuse() {
        super.prepareForReuse()
        self.posterImageView?.image = nil
        self.titleLabelView?.text = nil
        self.yearLabelView?.text = nil
    }
}
