//
//  ImageClient.swift
//  MovieLookupKit
//
//  Created by Derek Stutsman on 5/16/19.
//  Copyright © 2019 Stutsman Software. All rights reserved.
//

import UIKit

public typealias ImageCompletion = (UIImage?, Error?) -> Void

public class ImageClient: NSObject {

    //MARK: - Properties
    public static let sharedInstance: ImageClient = ImageClient()
    private var urlSession = URLSession(configuration: URLSessionConfiguration.default)
    private var imageCache = NSCache<AnyObject, UIImage>()
    
    //MARK: - Init/Dealloc
    public override init() {
        super.init()
        
        //Set up 10MB disk cache
        let urlCache = URLCache(memoryCapacity: 0, diskCapacity: 1024 * 1024 * 10, diskPath: "ImageCache")
        
        //Set it up for the URL session
        let config = URLSessionConfiguration.default
        config.urlCache = urlCache
        self.urlSession = URLSession(configuration: config)
    }
    
    //MARK: - Private HTTP Plumbing
    func httpGetImage(url: URL, completion:@escaping ImageCompletion) {
        //Start the network spinner
        NetworkActivity.sharedInstance.add()
        
        //Set up the data task
        self.urlSession.dataTask(with: url, completionHandler: {
            [weak self]
            (data, urlResponse, error) in
            
            //Kill the spinner
            NetworkActivity.sharedInstance.remove()
            
            //Ensure we got a valid image
            guard let data=data,
                let image = UIImage(data:data) else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
                return
            }
            
            //Cache it
            self?.imageCache.setObject(image, forKey: url.absoluteString as AnyObject)
            
            //Done
            DispatchQueue.main.async {
                completion(image, error)
            }
        }).resume()
    }

    //MARK: - Public methods
    public func clearCache() {
        print("ImageClient: Clearing cache")
        self.imageCache.removeAllObjects()
    }
    
    public func fetchImage(url:URL, completion:@escaping ImageCompletion) {
        //If the image is bad, return empty image
        if url.scheme == nil {
            completion(nil, nil)
            return
        }
        
        //Short-circuit: if the image is in cache, return it
        if let cachedImage = imageCache.object(forKey: url.absoluteString as AnyObject) {
            completion(cachedImage, nil)
            //print("ImageClient: Memory Cache HIT \(url)")
            return
        }
        
        //No hit, time to download it
        //print("ImageClient: Memory Cache MISS \(url)")
        self.httpGetImage(url: url, completion: completion)
    }
    
}
