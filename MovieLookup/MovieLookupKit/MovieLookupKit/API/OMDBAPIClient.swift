//
//  OMDBAPIClient.swift
//  MovieLookupKit
//
//  Created by Derek Stutsman on 5/16/19.
//  Copyright © 2019 Stutsman Software. All rights reserved.
//

import UIKit

public typealias OMDBSearchCompletion = (SearchResult?, Error?) -> Void
public typealias OMDBMovieCompletion = (Movie?, Error?) -> Void

public class OMDBAPIClient: NSObject {
    //Mark: - Constants
    private let OMDBAPIRoot = "https://www.omdbapi.com/?apikey=afb29973&type=movie" //Proprietary do not distribute

    //MARK: - Properties
    public static let sharedInstance: OMDBAPIClient = OMDBAPIClient()
    private let urlSession = URLSession(configuration: URLSessionConfiguration.ephemeral)
    
    //MARK: - Init/Dealloc
    public override init() {
        super.init()
    }
    
    //MARK: - Private HTTP Plumbing
    func httpGet(url: URL, completion:@escaping (Data?, Error?) -> Void) {
        //Start the network spinner
        NetworkActivity.sharedInstance.add()

        //Set up the data task
        self.urlSession.dataTask(with: url, completionHandler: {
            (data, urlResponse, error) in
            
            //Kill the spinner
            NetworkActivity.sharedInstance.remove()
            
            //Call completion
            completion(data, error)
        }).resume()
    }
    
    //MARK: - Public
    public func search(searchTerm: String, page:Int, completion: @escaping OMDBSearchCompletion) {
        
        //Attempt to create the URL, bail on fail
        guard let fixedSearchTerm:String = searchTerm.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed),
            let url = URL(string:"\(self.OMDBAPIRoot)&s=\(fixedSearchTerm)&page=\(page)") else {
            let error = NSError.init(domain: NSURLErrorDomain, code: 991, userInfo: [NSLocalizedDescriptionKey:"Search term failed to generate a valid URL"])
            DispatchQueue.main.async {
                completion(nil, error)
            }
            return
        }
        
        //Do the network fetch
        self.httpGet(url: url) {(data, error) in
            
            //Sanity check
            guard let data=data else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
                return
            }
            
            //Log out the data
            if let dataString = String(data: data, encoding: .utf8) {
                print("OMDBAPIClient search returned \(dataString)")
            }
            
            //Parse the JSON
            assert(!Thread.isMainThread, "No main thread for JSON parsing!")
            var result: SearchResult?
            var parseError: Error?
            do {
                let decoder = JSONDecoder()
                result = try decoder.decode(SearchResult.self, from: data)
            } catch let err {
                parseError = err
            }
            
            //Report the results on the main thread
            DispatchQueue.main.async {
                completion(result, parseError)
            }
        }
    }
    
    public func fetchMovieDetails(movieID: String, completion: @escaping OMDBMovieCompletion) {
        
        //Attempt to create the URL, bail on fail
        guard let url = URL(string:"\(self.OMDBAPIRoot)&i=\(movieID)") else {
            let error = NSError.init(domain: NSURLErrorDomain, code: 992, userInfo: [NSLocalizedDescriptionKey:"Movie fetch failed to generate a valid URL"])
            DispatchQueue.main.async {
                completion(nil, error)
            }
            return
        }

        //Do the network fetch
        self.httpGet(url: url) {(data, error) in
            
            //Sanity checks
            assert(!Thread.isMainThread, "No main thread for JSON parsing!")
            guard let data=data else {
                completion(nil, error)
                return
            }
            
            //Parse the JSON
            var result: Movie?
            var parseError: Error?
            do {
                let decoder = JSONDecoder()
                result = try decoder.decode(Movie.self, from: data)
            } catch let err {
                parseError = err
            }
            
            //Report the results
            DispatchQueue.main.async {
                completion(result, parseError)
            }
        }
    }
}
