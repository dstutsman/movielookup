//
//  SearchResult.swift
//  MovieLookupKit
//
//  Created by Derek Stutsman on 5/16/19.
//  Copyright © 2019 Stutsman Software. All rights reserved.
//

import UIKit

public struct SearchResult: Decodable {
    //MARK: - Properties
    public let movies:[MovieSearchResult]
    public let totalResults: String
    
    //MARK: - property/json key map
    enum CodingKeys : String, CodingKey {
        case movies = "Search"
        case totalResults
    }
}
