//
//  MovieSearchResult.swift
//  MovieLookupKit
//
//  Created by Derek Stutsman on 5/16/19.
//  Copyright © 2019 Stutsman Software. All rights reserved.
//

import UIKit

public struct MovieSearchResult: Decodable {
    //MARK: - Properties
    public let title: String
    public let posterURL: URL
    public let type: String
    public let year: String
    public let imdbID: String

    //MARK: - property/json key map
    enum CodingKeys : String, CodingKey {
        case title = "Title"
        case posterURL = "Poster"
        case type = "Type"
        case year = "Year"
        case imdbID
    }
}
