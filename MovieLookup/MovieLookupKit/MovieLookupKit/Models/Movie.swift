//
//  Movie.swift
//  MovieLookupKit
//
//  Created by Derek Stutsman on 5/16/19.
//  Copyright © 2019 Stutsman Software. All rights reserved.
//

import UIKit

public struct Movie: Decodable {
    //MARK: - Properties
    public let title: String
    public let releaseYear: String //Would like to be int, but returned as string from API
    public let rated: String
    public let releaseDate: String //TODO: Convert to proper date
    public let runtime: String
    public let genre: String
    public let director: String
    public let writer: String
    public let actors: String
    public let plot: String
    public let language: String
    public let country: String
    public let awards: String
    public let posterURL: URL
    public let ratings: [Rating]
    public let metascore: String
    public let imdbRating: String
    public let imdbVotes: String
    public let imdbID: String
    public let type: String
    public let dvdReleaseDate: String
    public let boxOffice: String
    public let production: String
    public let websiteURL: URL

    //MARK: - property/json key map
    enum CodingKeys : String, CodingKey {
        case title = "Title"
        case releaseYear = "Year"
        case rated = "Rated"
        case releaseDate = "Released"
        case runtime = "Runtime"
        case genre = "Genre"
        case director = "Director"
        case writer = "Writer"
        case actors = "Actors"
        case plot = "Plot"
        case language = "Language"
        case country = "Country"
        case awards = "Awards"
        case posterURL = "Poster"
        case ratings = "Ratings"
        case metascore = "Metascore"
        case imdbRating
        case imdbVotes
        case imdbID
        case type = "Type"
        case dvdReleaseDate = "DVD"
        case boxOffice = "BoxOffice"
        case production = "Production"
        case websiteURL = "Website"
    }
}
