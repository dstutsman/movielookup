//
//  Rating.swift
//  MovieLookupKit
//
//  Created by Derek Stutsman on 5/16/19.
//  Copyright © 2019 Stutsman Software. All rights reserved.
//

import UIKit

public struct Rating: Decodable {
    //MARK: - Properties
    public let source: String
    public let value: String
    
    //MARK: - property/json key map
    enum CodingKeys : String, CodingKey {
        case source = "Source"
        case value = "Value"
    }
}
