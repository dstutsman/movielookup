//
//  NetworkActivity.swift
//  MovieLookupKit
//
//  Created by Derek Stutsman on 5/16/19.
//  Copyright © 2019 Stutsman Software. All rights reserved.
//

//Manages a thread-safe counter for network activity
public class NetworkActivity: NSObject {
    
    //MARK: - Internal
    private let serialQueue = DispatchQueue(label: "Atomic network counter queue")
    private var networkOperations = 0
    public var display: ((Bool) -> Void)?
    
    //MARK: - Properties
    public static let sharedInstance: NetworkActivity = NetworkActivity()
    
    //MARK: - Private
    private func updateCounter(increment: Int) {
        //Operate on the counter in a thread-safe way
        serialQueue.async{
            //Bump the counter up or down
            self.networkOperations += increment

            //Determine active state
            let networkActive = self.networkOperations > 0
            
            //Update the UI on the main thread - we don't do it directly here as MovieLookupKit should not have UI knowledge, so we just call the closure hooked by the UI
            if let displayClosure = self.display {
                DispatchQueue.main.async {
                    displayClosure(networkActive)
                }
            }
        }
    }

    public func add() {
        self.updateCounter(increment: 1)
    }
    
    public func remove() {
        self.updateCounter(increment: -1)
    }
    
    //MARK: - Init/Deinit
    public override init() {
        super.init()
    }
}
