//
//  MovieLookupKit.h
//  MovieLookupKit
//
//  Created by Derek Stutsman on 5/16/19.
//  Copyright © 2019 Stutsman Software. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MovieLookupKit.
FOUNDATION_EXPORT double MovieLookupKitVersionNumber;

//! Project version string for MovieLookupKit.
FOUNDATION_EXPORT const unsigned char MovieLookupKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MovieLookupKit/PublicHeader.h>


