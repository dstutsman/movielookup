//
//  MovieLookupKitTests.swift
//  MovieLookupKitTests
//
//  Created by Derek Stutsman on 5/16/19.
//  Copyright © 2019 Stutsman Software. All rights reserved.
//

import XCTest
@testable import MovieLookupKit

class MovieLookupKitTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testMovieSearch() {
        let expectation = XCTestExpectation(description: "Execute search API")
        OMDBAPIClient.sharedInstance.search(searchTerm: "star", page: 1) { (result, error) in
            XCTAssert(result != nil)
            XCTAssert(error == nil)
            //TODO: Add more test checks, rudimentary for now
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testMovieLookup() {
        let expectation = XCTestExpectation(description: "Execute movie lookup API")
        OMDBAPIClient.sharedInstance.fetchMovieDetails(movieID: "tt0076759") { (movie, error) in
            XCTAssert(movie != nil)
            XCTAssert(error == nil)
            //TODO: Add more test checks, rudimentary for now
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 20.0)
    }

}
