//
//  MovieDetailsViewController.swift
//  MovieLookupTV
//
//  Created by Derek Stutsman on 8/25/19.
//  Copyright © 2019 Stutsman Software, LLC. All rights reserved.
//

import UIKit
import MovieLookupKitTV

class MovieDetailsViewController: UIViewController {
    //MARK: - Properties
    public var movieID: String?
    @IBOutlet weak public var spinner:UIActivityIndicatorView?
    @IBOutlet weak public var imageView:UIImageView?
    @IBOutlet weak public var titleLabel:UILabel?
    @IBOutlet weak public var yearLabel:UILabel?
    @IBOutlet weak public var plotLabel:UILabel?
    
    private var movie: Movie?
    
    //MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Load details
        self.loadMovieDetails()
    }
    
    //MARK: - Private I/O
    private func loadMovieDetails() {
        //Sanity check - if we don't have a movie ID for some reason just pop back
        guard let movieID = self.movieID else {
            self.dismiss(animated: true) {
                
            }
            return
        }
        
        //Do the fetch
        OMDBAPIClient.sharedInstance.fetchMovieDetails(movieID: movieID) { (movie, error) in
            //Sanity check the result
            guard let movie=movie else {
                //Failed!
                //TODO: Show error
                return
            }
            
            //Save it
            self.movie = movie
            
            //Update the UI
            self.loadMovieUI()
        }
    }
    
    private func loadMovieUI() {
        //Sanity check
        guard let movie=self.movie else {
            return
        }
        
        //Load image
        ImageClient.sharedInstance.fetchImage(url: movie.posterURL) { (image, error) in
            self.imageView?.image = image
            self.spinner?.stopAnimating()
        }
        
        //Load title etc
        self.titleLabel?.text = movie.title
        self.yearLabel?.text = movie.releaseYear
        self.plotLabel?.text = movie.plot
    }
}
