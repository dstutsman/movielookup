//
//  AppDelegate.swift
//  MovieLookupTV
//
//  Created by Derek Stutsman on 8/25/19.
//  Copyright © 2019 Stutsman Software, LLC. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    //MARK: - UIApplicationDelegate
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //Create main window
        window = UIWindow(frame: UIScreen.main.bounds)
        
        //Set up nav controller and search container
        let navController = createRootController()
        window?.rootViewController = navController
        window?.addSubview(navController.view)
        window?.makeKeyAndVisible()
        return true
    }
    
    //MARK: - Nav controller setup
    func createRootController() -> UINavigationController {
        //Set up results controller (our custom part of the UI)
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let searchResultsController = storyboard.instantiateViewController(withIdentifier: SearchResultsViewController.storyboardIdentifier()) as? SearchResultsViewController

        //Wrap it in Apple search controller which also responds to updates
        let searchController = UISearchController(searchResultsController: searchResultsController)
        searchController.searchResultsUpdater = searchResultsController
        searchController.searchBar.placeholder = NSLocalizedString("Enter movie title", comment: "")
        
        //Wrap _that_ in Apple's container
        let searchContainer = UISearchContainerViewController(searchController: searchController)
        searchContainer.title = NSLocalizedString("Search", comment: "")
        
        //Finally wrap the whole thing in a nav controller
        let navigationController = UINavigationController(rootViewController: searchContainer)
        return navigationController
    }
}

