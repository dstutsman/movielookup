//
//  SearchResultsViewController.swift
//  MovieLookupTV
//
//  Created by Derek Stutsman on 8/25/19.
//  Copyright © 2019 Stutsman Software, LLC. All rights reserved.
//

import UIKit
import MovieLookupKitTV

class SearchResultsViewController: UIViewController, UISearchResultsUpdating, UICollectionViewDataSource {
    
    //MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var noResultsLabel: UILabel?
    
    //MARK: - Properties
    var resultsPage = 1
    var lastSearchTerm : String?
    var lastSearchResult : SearchResult?
    var movies : [MovieSearchResult] = []
    
    //MARK: - Static
    static func storyboardIdentifier() -> String {
        return "SearchResultsViewController"
    }

    //MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.clipsToBounds = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        //Pass movie ID to detail controller
        if let detailController = segue.destination as? MovieDetailsViewController,
           let clickedCell = sender as? SearchResultCollectionViewCell,
           let indexPath = collectionView?.indexPath(for: clickedCell) {
            let movie = movies[indexPath.row]
            detailController.movieID = movie.imdbID
        }
    }
    
    //MARK: - UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        //Capture existing searchbar text
        guard let searchTerm = searchController.searchBar.text else {
            return
        }
        
        //If it hasn't changed after 0.5 seconds, do the search
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if searchController.searchBar.text == searchTerm {
                self.resultsPage = 1
                self.performSearch(searchTerm: searchTerm)
            }
        }
    }
    
    //MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //Dequeue the cell
        let cell:SearchResultCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchResultCollectionViewCell.identifier, for: indexPath) as! SearchResultCollectionViewCell
        
        //Grab the movie
        let movie = self.movies[indexPath.row]
        
        //Populate the cell
        cell.titleLabelView!.text = movie.title
        
        //Fetch the image, taking care to ensure it hasn't changed
        cell.tag = indexPath.row
        cell.titleLabelView?.isHidden = false
        cell.posterImageView?.image = UIImage(named: "placeholder")
        if (movie.posterURL.scheme != nil) {
            ImageClient.sharedInstance.fetchImage(url: movie.posterURL) { (image, error) in
                if cell.tag == indexPath.row {
                    if image != nil {
                        cell.posterImageView?.image = image
                        cell.posterImageView?.setNeedsDisplay()
                        cell.titleLabelView?.isHidden = true
                    } else {
                        cell.titleLabelView?.isHidden = false
                    }
                }
            }
        }
        
        //If we lack the data to fetch more, just bail
        guard let result = self.lastSearchResult,
            let count = Int(result.totalResults),
            let searchTerm = lastSearchTerm else {
                return cell
        }

        //Fetch more on the last row
        if self.movies.count < count && indexPath.row == self.movies.count-1 {
            print("SearchViewController: Fetching more data, \(count) total, \(movies.count) so far")
            self.resultsPage += 1
            self.performSearch(searchTerm: searchTerm)
        }
        
        //Done
        return cell
    }
    
    //MARK: - Private I/O
    private func performSearch(searchTerm: String) {
        //Ignore if search term is duplicate - prevents double-search for the same term when typing space, h
        if self.resultsPage == 1 && self.lastSearchTerm != nil && self.lastSearchTerm?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == searchTerm.trimmingCharacters(in: .whitespacesAndNewlines) {
            print("Ignoring duplicate search term")
            return
        }
        
        //Save search term for next time
        self.lastSearchTerm = searchTerm
        
        //Clear existing data on first page
        if self.resultsPage == 1 {
            self.movies.removeAll()
            self.collectionView?.reloadData()
        }
        
        OMDBAPIClient.sharedInstance.search(searchTerm: searchTerm, page: self.resultsPage) {
            [weak self]
            (searchResult, error) in
            
            //Sanity check
            guard let strongSelf = self else {
                return
            }
            
            //Handle error?
            if error != nil {
                print("FETCH ERROR: \(error!)")
                strongSelf.collectionView?.isHidden = true
                strongSelf.noResultsLabel?.isHidden = false
                //TODO: Differentiate between "no results" parsing errors, and actual network errors.
                //Display network errors with an alert
                return
            }
            
            //Sanity checks / crack optionals
            if let searchResult=searchResult {
                //Save search result
                strongSelf.lastSearchResult = searchResult
                
                //Create the array of indexPaths
                var insertPaths = [IndexPath]()
                for i in strongSelf.movies.count ... (strongSelf.movies.count + searchResult.movies.count)-1 {
                    insertPaths.append(IndexPath(item: i, section: 0))
                }
                
                //Add to the list, or clear it
                if strongSelf.movies.count == 0 {
                    //First load
                    strongSelf.movies = searchResult.movies
                    strongSelf.collectionView?.reloadData()
                } else {
                    //Second+ load
                    strongSelf.movies.append(contentsOf: searchResult.movies)
                    strongSelf.collectionView?.insertItems(at: insertPaths)
                }
                
                //Show tableView and hide "no results" label
                strongSelf.collectionView?.isHidden = false
                strongSelf.noResultsLabel?.isHidden = true
            } else {
                //No results
                strongSelf.collectionView?.isHidden = true
                strongSelf.noResultsLabel?.isHidden = false
            }
        }
    }
}
