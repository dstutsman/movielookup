//
//  SearchResultCollectionViewCell.swift
//  MovieLookupTV
//
//  Created by Derek Stutsman on 8/25/19.
//  Copyright © 2019 Stutsman Software, LLC. All rights reserved.
//

import UIKit

class SearchResultCollectionViewCell: UICollectionViewCell {
    //MARK: - Properties
    public static let identifier = "searchResultCell"
    @IBOutlet weak var posterImageView: UIImageView?
    @IBOutlet weak var titleLabelView: UILabel?
    
    //MARK: - UIView
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //Move the label to the image view's overlay so it sizes with focus
        if let titleLabelView = titleLabelView {
            posterImageView?.overlayContentView.addSubview(titleLabelView)
        }
    }
    
    //MARK: - UITableViewCell
    override func prepareForReuse() {
        super.prepareForReuse()
        self.posterImageView?.image = nil
        self.titleLabelView?.text = nil
        self.titleLabelView?.isHidden = true
        self.tag = -1
    }
}
