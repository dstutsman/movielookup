Movie Lookup Overview
=====================

This app is a simple code sample showing how I like to build and structure applications.
This sample uses Swift 5 and covers a variety of common bread-and-butter iOS development tasks including:

* Networking to a 3rd party web service (http://www.omdbapi.com)
* JSON parsing into model objects
* Thread and queue management
* Rudimentary UI using autolayout
* Clients for iOS and tvOS

At the moment the features and interface are spartan.  
You can type the name of a movie to view matching hits, and tap one to drill into detail on it.

OMDB is an imperfect API.  Search is weak with partial terms. Some of the content lacks art, for example, and I've found a fair number of dupes.
It's servicable enough for this sample, but on a production app I'd seek out something more consistent.

This sample may be extended and improved over time.

Architecture and Design Intent
==============================

The app is broken down into three major components:

* MovieLookupKit - This layer is responsible for networking, JSON parsing, models, business logic, etc.  It is shared between iOS and tvOS.
* MovieLookup - The iOS UI project that consumes MovieLookupKit.
* MovieLookupTV - The tvOS UI project that consumes MovieLookupKit.

I almost always build iOS apps using this approach of separating the non-visual guts from the UI presentation.
This has afforded me great flexibility over time...for example when a new device/platform comes out, or the UI paradigms shift as they did with iOS 7.

The UI layer essentially becomes a thin, simple "snap off" piece easy to replace with something entirely new without losing the investment in the non-visual work.

Third Party Components
======================

I am aware of CocoaPods, Carthage and so on, but for this sample I explictly chose not to use them.
As a rule I try to avoid third party components unless they solve a very big problem.  Networking, caching etc are well solved by the built in frameworks in my view.

In particular I find the new(ish) "Decodable" paradigm for JSON deserialization extremely elegant.

Building the App
================

* Open the MovieLookup.xcworkspace
* Select the MovieLookup project in Xcode and a simulator of your choice (I targeted the iPhone Xr primarily.)
* Select the MovieLookupTV project in Xcode and a tvOS simulator of your choice (I targeted AppleTV 4K @1080)

Ideas for Improvement
=====================

Some things I'd like to do to enhance this sample with more time:

* Use Core Data to cache the models.  I'm expert with Core Data and know the sharp edges to avoid, and how to effectively use it without blocking the main thread.
* Add more details to the movie detail screen.  
* Change from the static tableView to a scrollView with all the controls flowing relative to one other.
* Improve the UI appearance in general
* Add links to other services for buying movie tickets, etc.
* Sort and dedupe results

Questions and Comments
======================

Thanks for reviewing my sample!  Hit me up at derek@stutsmansoft.com to talk about it.
